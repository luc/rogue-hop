# vim:set sw=4 ts=4 sts=4 ft=perl expandtab:
package LastCustom;
use Mojo::Base -strict;

sub custom_filter {
    my $content = shift;
    my $id      = shift;

    # Exemple of modification:
    $content =~ s/pendant de temps/pendant le temps/g if $id == 100009376018174573;
    $content =~ s#<a href="https://framapiaf.org/media[^"]"*><span class="invisible">[^<]*</span><span class="ellipsis">[^<]*</span><span class="invisible">[^<]*</span></a>">##g;

    return $content;
}

1;
